import astra.ast.core.*;
import astra.ast.statement.WaitStatement;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestStatements {
	private ASTRAParser parser;
	private ADTTokenizer tokenizer;

	private List<Token> setup(String input) throws ParseException {
        tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
        parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}
        
        return list;
    }

	@Test
	public void waitFormulaTest() throws ParseException {
		List<Token> tokens = setup("wait(happy())");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNull(statement.timeout());
	}

	@Test(expected = ParseException.class)
	public void waitTermTest() throws ParseException {
		List<Token> tokens = setup("wait(\"happy\")");
		parser.createStatement(tokens);
	}

	@Test(expected = ParseException.class)
	public void doneErrorTest() throws ParseException {
		List<Token> tokens = setup("done(");
		parser.createStatement(tokens);
	}

	@Test
	public void doneOKTest() throws ParseException {
		List<Token> tokens = setup("done");
		parser.createStatement(tokens);
	}

	@Test(expected = ParseException.class)
	public void waiNoBracketTest() throws ParseException {
		List<Token> tokens = setup("wait(happy()");
		parser.createStatement(tokens);
	}

	@Test
	public void waitTimeoutTest() throws ParseException {
		List<Token> tokens = setup("wait(happy(), 1000)");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNotNull(statement.timeout());
	}

	@Test
	public void waitTrueTest() throws ParseException {
		List<Token> tokens = setup("wait(true, 1000)");
		IStatement term = parser.createStatement(tokens);
		assertEquals(WaitStatement.class, term.getClass());
		WaitStatement statement = (WaitStatement) term;
		assertNotNull(statement.guard());
		assertNotNull(statement.timeout());
	}

	@Test(expected = ParseException.class)
	public void waitTimeoutNoBracketTest() throws ParseException {
		List<Token> tokens = setup("wait(happy(), 1000");
		parser.createStatement(tokens);
	}
}
