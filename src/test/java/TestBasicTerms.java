import astra.ast.core.*;
import astra.ast.term.CountFormulaeTerm;
import astra.ast.term.Function;
import astra.ast.term.Literal;
import astra.ast.term.ModuleTerm;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;

public class TestBasicTerms {
	ASTRAParser parser;
	ADTTokenizer tokenizer;

	public List<Token> setup(String input) throws ParseException {
		tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
		parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<Token>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}

		return list;
	}

	@Test
	public void stringTest() throws ParseException {
		List<Token> tokens = setup("\"name\"");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test(expected = ParseException.class)
	public void missingQuoteStringTest() throws ParseException {
		List<Token> tokens = setup("\"name");
		parser.createTerm(tokens);
	}

	@Test
	public void characterTest() throws ParseException {
		List<Token> tokens = setup("'c'");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test(expected = ParseException.class)
	public void missingQuoteCharacterTest() throws ParseException {
		List<Token> tokens = setup("'c");
		parser.createTerm(tokens);
	}

	@Test
	public void intTest() throws ParseException {
		List<Token> tokens = setup("12");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void longTest() throws ParseException {
		List<Token> tokens = setup("12l");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void floatTest() throws ParseException {
		List<Token> tokens = setup("12.0f");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void doubleTest() throws ParseException {
		List<Token> tokens = setup("12.0");
		System.out.println("tokens: " + tokens);
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test
	public void functionTest() throws ParseException {
		List<Token> tokens = setup("fatherOf(\"rem\")");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Function.class, term.getClass());
	}

	@Test(expected = ParseException.class)
	public void failedFunctionTest() throws ParseException {
		List<Token> tokens = setup("fatherOf(\"rem\"");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Function.class, term.getClass());
	}

	@Test
	public void moduleTermTest() throws ParseException {
		List<Token> tokens = setup("mod.fatherOf(\"rem\")");
		ITerm term = parser.createTerm(tokens);
		assertEquals(ModuleTerm.class, term.getClass());
	}

	@Test
	public void jsonTest() throws ParseException {
		List<Token> tokens = setup("{ \"name\":\"Rem\" }");
		ITerm term = parser.createTerm(tokens);
		assertEquals(Literal.class, term.getClass());
	}

	@Test(expected = ParseException.class)
	public void missingBracketJsonTest() throws ParseException {
		List<Token> tokens = setup("{ \"name\":\"Rem\"");
		parser.createTerm(tokens);
	}

	@Test(expected = ParseException.class)
	public void invalidJsonTest() throws ParseException {
		List<Token> tokens = setup("{ \"name\" \"Rem\"} ");
		parser.createTerm(tokens);
	}

	@Test
	public void countFormulaeTest() throws ParseException {
		List<Token> tokens = setup("count_formulae(init(happy))");
		ITerm term = parser.createTerm(tokens);
		assertEquals(CountFormulaeTerm.class, term.getClass());
	}

	@Test(expected = ParseException.class)
	public void invalidCountFormulaeTest() throws ParseException {
		List<Token> tokens = setup("count_formulae(init)");
		ITerm term = parser.createTerm(tokens);
		assertEquals(CountFormulaeTerm.class, term.getClass());
	}

}
