package astra.compiler;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import astra.ast.core.IJavaHelper;
import astra.ast.core.NoSuchASTRAClassException;
import astra.ast.core.ParseException;
import astra.ast.reflection.ReflectionHelper;

public class ASTRACompiler {
	IJavaHelper helper;
	ASTRAClassHierarchy hierarchy;

	public static void main(String[] args) {
//		System.out.println("+++++++++++++++++++++++++++++++++++++++++ START OF COMPILATION +++++++++++++++++++++++++++++++++++++++++");
		if (args.length == 1) {
			File source = new File(args[0]);
			if (!source.exists()) {
				System.out.println("[ASTRACompiler] Invalid Source Folder: " + args[0] + " / full path: " + source.getAbsolutePath());
				System.exit(1);
			}
			recursiveCompile(ASTRACompiler.newInstance(source), source, "");
		} else if (args.length == 2) {
			File source = new File(args[0]);
			if (!source.exists()) {
				System.out.println("[ASTRACompiler] Invalid Source Folder: " + args[0] + " / full path: " + source.getAbsolutePath());
				System.exit(1);
			}
			File target = new File(args[1]);
			if (!target.exists()) {
				target.mkdirs();
				System.out.println("[ASTRACompiler] Creating Target Folder: " + args[1] + " / full path: " + target.getAbsolutePath());
			}
			try {
				recursiveCompile(ASTRACompiler.newInstance(source, target), source, "");
			} catch (NoSuchASTRAClassException e) {
				System.out.println("[ASTRACompiler] An error was encountered when attempting to load one of the specified classes:");
				System.out.println("[ASTRACompiler] " + e.getMessage());

			}
		} else {
			System.out.println("[ASTRACompiler] ASTRA Compiler requires at least 1 argument: <source-folder> [<target-folder>]");
			System.exit(1);
		}
//		System.out.println("++++++++++++++++++++++++++++++++++++++++++ END OF COMPILATION ++++++++++++++++++++++++++++++++++++++++++");
	}	

	private static void recursiveCompile(ASTRACompiler compiler, File input, String path) {
//		System.out.println("EXPLORING: " + input.getAbsolutePath());
//		System.out.println("Files: " + Arrays.asList(input.listFiles()));
		for (File file : input.listFiles()) {
			if (file.isFile()) {
				if (file.getName().endsWith(".astra")) {
					String filename = file.getName();
					filename = filename.substring(0, filename.lastIndexOf('.'));
					compiler.run_compiler(path + filename);
				}
			} else {
				// file is a folder...
				recursiveCompile(compiler, file, path + file.getName() + ".");
			}
		}

	}

	public static void compile(String source) {
		ASTRACompiler.newInstance().run_compiler(source);
	}

	public static ASTRACompiler newInstance() {
		return newInstance(new File("src/"));
	}

	public static ASTRACompiler newInstance(File source) {
		return newInstance(source, source);
	}

	public static ASTRACompiler newInstance(File source, File target) {
		return new ASTRACompiler(source, target);
	}

	private ASTRACompiler(File source, File target) {
		helper = new ReflectionHelper(source, target);
		hierarchy = new ASTRAClassHierarchy(helper);
	}

	public void run_compiler(String cls) {
		Map<String, List<ParseException>> errors = new HashMap<String, List<ParseException>>();
		// System.out.println("[ASTRACompiler] Compile: " + cls);
		hierarchy.compile(cls, errors);
		if (!errors.isEmpty()) {
			boolean error = false;
			for (Entry<String, List<ParseException>> entry : errors.entrySet()) {
				for (ParseException e : entry.getValue()) {
					error = true;
					System.out.println("["+entry.getKey()+"] " + e.getMessage());
//					e.printStackTrace();
				}
			}
			if (error) {
				System.out.println("[ASTRACompiler] Exiting due to unexpected compile errors");
				System.exit(1);
			}
		}
	}
}
