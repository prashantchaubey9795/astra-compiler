package astra.ast.term;

import astra.ast.core.*;
import astra.ast.type.ObjectType;

public class FromJsonTerm extends AbstractElement implements ITerm {
	ITerm term;
	String type;

	public FromJsonTerm(ITerm term, String type, Token start, Token end, String source) {
		super(start, end, source);
		this.term = term;
		this.type = type;
	}

	public ITerm term() {
		return term;
	}
	
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String toString() {
		return term.toString();
	}

	public IType type() {
		return new ObjectType(Token.OBJECT, type);
	}

	public String specifiedType() {
		return type;
	}
}
