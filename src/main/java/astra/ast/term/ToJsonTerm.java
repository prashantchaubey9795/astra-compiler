package astra.ast.term;

import astra.ast.core.*;
import astra.ast.type.BasicType;

public class ToJsonTerm extends AbstractElement implements ITerm {
	ITerm term;

	public ToJsonTerm(ITerm term, Token start, Token end, String source) {
		super(start, end, source);
		this.term = term;
	}

	public ITerm term() {
		return term;
	}
	
	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public String toString() {
		return "to_json("+term.toString()+")";
	}

	public IType type() {
		return new BasicType(Token.STRING);
	}
}
