package astra.ast.type;

import astra.ast.core.IElement;
import astra.ast.core.IElementVisitor;
import astra.ast.core.IType;
import astra.ast.core.ParseException;
import astra.ast.core.Token;

public class BasicType implements IType {
	int type;
	private IElement parent;
	
	public BasicType(int type) {
		this.type = type;
	}

	public Object accept(IElementVisitor visitor, Object data) throws ParseException {
		return visitor.visit(this, data);
	}

	public int type() {
		return type;
	}

	public String getSource() {
		return null;
	}
	
	public boolean equals(Object object) {
		if (object instanceof BasicType) {
			return type == ((BasicType) object).type;
		}
		return false;
	}
	
	public String toString() {
		return Token.toTypeString(type);
	}

	public IElement[] getElements() {
		return new IElement[0];
	}

	public IElement getParent() {
		return parent;
	}

	public IElement setParent(IElement parent) {
		this.parent =parent;
		return this;
	}
	
	public int getBeginLine() {
		return parent.getBeginLine();
	}

	public int getBeginColumn() {
		return parent.getBeginColumn();
	}

	public int charStart() {
		return 0;
	}

	public int charEnd() {
		return 0;
	}
}
